import Vue from 'vue';
import { Player } from '../types/Player';
import { StateType } from "./types";
import Vuex, { ActionContext } from "vuex";

Vue.use(Vuex);


export default new Vuex.Store({
  state: {
    players: [
      {
        name: 'dusa',
        power: 4,
        health: 100,
        status: 'ally',
      },
      {
        name: 'alonga',
        power: 3,
        health: 100,
        status: 'enemy'
      }
    ]
  },
  mutations: {
    CHANGE_PLAYERS(state: StateType, newPlayers: Player[]) : void {
      state.players = newPlayers;
    }
  },
  actions: {
    changeHealthByName(context: ActionContext<StateType, StateType>,
       hitInfo: { playerName: string, damage: Number }) : void {
      context.commit('CHANGE_PLAYERS', context.getters.getPlayers().map( (player: Player) => {
        if(player.name === hitInfo.playerName) {
          player.health =+ hitInfo.damage
        }
      }))
    }
  },
  getters: {
    getPlayers(state: StateType): Player[] {
      return state.players
    }
  },
});
