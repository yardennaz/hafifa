import { Player } from '../types/Player'

export interface StateType {
    players: Player[]    
}