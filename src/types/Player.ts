export type Player  = {
    name: string,
    health: number,
    power: number,
    status: string,
}