export type Log = {
  damage?: number,
  playerStatus?: string,
  activePlayerName?: string,
  passivePlayerName?: string,
  action?: string
}
